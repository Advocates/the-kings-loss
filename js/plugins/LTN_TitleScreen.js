//=============================================================================
//LTN_Title.js
//=============================================================================
/*:
* @plugindesc V1.0 TitleScene.
*
* @author LTN Games
*
*
*
* @help
================================================================================
TERMS OF USE
================================================================================
Credit goes to: LTN Games

Exclusive to rpgmakermv.co, please don't share anywhere else unless given strict
permission by the author of the plugin.

The plugin may be used in commerical and non-commerical products.
Credit must be given!

Please report all bugs to http://www.rpgmakermv.co/threads/ScriptName

===============================================================================
INSTRUCTIONS
===============================================================================

*/

var LTN = LTN || {};
LTN.Title = LTN.Title || {};

(function (){
	LTN.Parameters = PluginManager.parameters('LTN_SQTitle');
	LTN.Param = LTN.Param || {};

	//=========================================================================
	// TouchInput - RollOver Sprites
	//=========================================================================
	TouchInput._onMouseMove = function(event) {
		var x = Graphics.pageToCanvasX(event.pageX);
		var y = Graphics.pageToCanvasY(event.pageY);
		this._onMove(x, y);
	};


	Window_TitleCommand.prototype.makeCommandList = function() {
		this.addCommand(TextManager.newGame,   'newGame');
		this.addCommand(TextManager.continue_, 'continue', this.isContinueEnabled());
		this.addCommand(TextManager.options,   'options');
		//this.addCommand('Quit',   'quit');
	};
	//=============================================================================
	// Scene_Title
	//=============================================================================
	var commandImages  = ['NewGame',  'Continue',  'Options'];
	//-----====------
	//Overwrite Method: createCommandWindow
	//-------------------------------------------------------
	Scene_Title.prototype.createCommandWindow = function() {
		this._commandWindow = new Window_TitleCommand();
		this._commandWindow.visible = false;
		this._commandWindow.x = Graphics.boxWidth;
		this._commandWindow.y = Graphics.boxHeight;
		this._commandWindow.close();
		this._commandWindow.setHandler('newGame',  this.commandNewGame.bind(this));
		this._commandWindow.setHandler('continue', this.commandContinue.bind(this));
		this._commandWindow.setHandler('options',  this.commandOptions.bind(this));
		//this._commandWindow.setHandler('quit',     this.commandExit.bind(this));
		this.addWindow(this._commandWindow);
	};
	//-----====------
	//Aliased Method: Create
	//-------------------------------------------------------
	var LTN_oldSceneTitle_Create = Scene_Title.prototype.create;
	//-------------------------------------------------------
	Scene_Title.prototype.create = function() {
		LTN_oldSceneTitle_Create.call(this);
		this.createCursor();
		this.createHighlight();
		this.drawSpriteButtons();
		this.setButtonCommands();
	};

	Scene_Title.prototype.createHighlight = function  () {
		this._highlight = new Sprite();
		this._highlight.bitmap = ImageManager.loadSystem('CommandCursor');
		this._highlight.visible = false;
		this.addChild(this._highlight);
	};

	Scene_Title.prototype.createCursor = function  () {
		this._spriteCursor = new Sprite();
		this._spriteCursor.bitmap = ImageManager.loadSystem('Cursor');
		this._spriteCursor.visible = false;
		this.addChild(this._spriteCursor);
	};

	//-----====------
	//Aliased Method: Create
	//-------------------------------------------------------
	var LTN_oldSceneTitle_Update = Scene_Title.prototype.update;
	//-------------------------------------------------------
	Scene_Title.prototype.update = function() {
		LTN_oldSceneTitle_Update.call(this);
		this.spriteButtonUpdate();
		this.updateCommands();
	};
	//----------------------------------------------------------------------------------
	// Update sprite buttons for detection of mouse rollover.
	//----------------------------------------------------------------------------------
	Scene_Title.prototype.spriteButtonUpdate = function() {
		//New Game
		if(this._spriteNewGame.isButtonTouched()){
			this._commandWindow._index = 0;
			//            SoundManager.playCursor();
		}
		if(this._spriteContinue.isButtonTouched()){
			this._commandWindow._index = 1;
			//            SoundManager.playCursor();
		}  
		if(this._spriteOptions.isButtonTouched()){
			this._commandWindow._index = 2;
			//            SoundManager.playCursor();
		}
		//if(this._spriteExit.isButtonTouched()){
		//    this._commandWindow._index = 3;
		//            SoundManager.playCursor();
		//}
	};

	Scene_Title.prototype.updateCommands = function() {
		if(this._commandWindow._index === 0){
			this._highlight.x = 25;
			this._highlight.y = this._spriteNewGame.y + this._spriteNewGame.height;
			this._highlight.visible = true;
			
			this._spriteCursor.x  = 0;
			this._spriteCursor.y  = this._spriteNewGame.y;
			this._spriteCursor.visible = true;
		}
		if(this._commandWindow._index === 1){
			this._highlight.x = 25;
			this._highlight.y = this._spriteContinue.y + this._spriteContinue.height;
			this._highlight.visible = true;
			
			this._spriteCursor.x  = 0;
			this._spriteCursor.y  = this._spriteContinue.y;
			this._spriteCursor.visible = true;
		}
		if(this._commandWindow._index === 2){
			this._highlight.x = 25;
			this._highlight.y = this._spriteOptions.y + this._spriteOptions.height;
			this._highlight.visible = true;
			
			this._spriteCursor.x  = 0;
			this._spriteCursor.y  = this._spriteOptions.y;
			this._spriteCursor.visible = true;
		}
		//if(this._commandWindow._index === 3){
		//    this._spriteCursor.x  = this._spriteExit.x - 20;
		//    this._spriteCursor.y  = this._spriteExit.y;
		//    this._spriteCursor.visible = true;
		//}

	};
	Scene_Title.prototype.drawSpriteButtons = function() {
		this.createNewGameButton();
		this.createContinueButton();
		this.createOptionsButton();
		//this.createExitButton();
		//this.createVersionText();
	};

	Scene_Title.prototype.setButtonCommands = function() {
		this._spriteNewGame.setClickHandler(this.commandNewGame.bind(this));
		this._spriteContinue.setClickHandler(this.commandContinue.bind(this));
		this._spriteOptions.setClickHandler(this.commandOptions.bind(this));
		//this._spriteExit.setClickHandler(this.commandExit.bind(this));
	};

	//Scene_Title.prototype.createVersionText = function() {
	//    this._spriteVersion   = new Sprite();
	//    this._spriteVersion.bitmap = ImageManager.loadSystem(commandImages[4]);
	//    this._spriteVersion.x = Graphics.width / 2;
	//    this._spriteVersion.y = 30;
	//    this.addChild(this._spriteVersion);
	//};
	Scene_Title.prototype.createNewGameButton = function() {
		this._spriteNewGame   = new Sprite_Button();
		this._spriteNewGame.bitmap = ImageManager.loadSystem(commandImages[0]);
		this._spriteNewGame.x = 25;
		this._spriteNewGame.y = Graphics.height / 2 + 100;
		this.addChild(this._spriteNewGame);
	};

	Scene_Title.prototype.createContinueButton = function() {
		this._spriteContinue   = new Sprite_Button();
		this._spriteContinue.bitmap = ImageManager.loadSystem(commandImages[1]);
		this._spriteContinue.x = 25;
		this._spriteContinue.y = Graphics.height / 2 + 180;
		this.addChild(this._spriteContinue);
	};

	Scene_Title.prototype.createOptionsButton = function() {
		this._spriteOptions   = new Sprite_Button();
		this._spriteOptions.bitmap = ImageManager.loadSystem(commandImages[2]);
		this._spriteOptions.x = 25;
		this._spriteOptions.y = Graphics.height / 2 + 265;
		this.addChild(this._spriteOptions);
	};



	//Scene_Title.prototype.createExitButton = function() {
	//    this._spriteExit   = new Sprite_Button();
	//    this._spriteExit.bitmap = ImageManager.loadSystem(commandImages[3]);
	//    this._spriteExit.x = 40;
	//    this._spriteExit.y = Graphics.height / 2 + 260;
	//    this.addChild(this._spriteExit);
	//};
	Scene_Title.prototype.commandNewGame = function() {
		SoundManager.playOk();
		DataManager.setupNewGame();
		this.fadeOutAll();
		SceneManager.goto(Scene_Map);
	};

	Scene_Title.prototype.commandContinue = function() {
		SoundManager.playOk();
		SceneManager.push(Scene_Load);
	};

	Scene_Title.prototype.commandOptions = function() {
		SoundManager.playOk();
		SceneManager.push(Scene_Options);
	};
	Scene_Title.prototype.commandExit = function() {
		SoundManager.playOk();
		SceneManager.exit();
	};
})();
