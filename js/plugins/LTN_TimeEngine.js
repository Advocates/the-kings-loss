//=============================================================================
//LTN_TimeEngine.js
//=============================================================================
// Version 1.0.1
/*:
 * @plugindesc v1.0.2 A simple but powerful time engine.
 <LTN_TimeEngine>

 * @author LTN Games

 @param ================
 @param  Game Time
 @param ================

 @param Time Speed
 @desc How fast should the time move? Higher the number slower the time.
 @default 2

 @param Seconds In A Minute
 @desc How many seconds in a minute? This will fine tune time speed for those
 looking for better adjustment of speed. Default: 60
 @default 60

 @param Minutes In An Hour
 @desc How many minutes in an hour?
 Default: 60
 @default 60

 @param Hours In A Day
 @desc How many hours in a day?
 Default: 24
 @default 24

 @param Days of the Week
 @desc The names of the days of the week
 @default Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday

 @param Days of the Week Abreviations
 @desc The names of the days of the week in abreviated form.
 @default Sun.,Mon.,Tues.,Wed.,Thurs.,Fri.,Sat.

 @param Month Names
 @desc The names of the months in a year.
 @default January,February,March,April,May,June,July,August,September,October,November,December

 @param Month Abreviations
 @desc The names of the months in a year in abreviated form.
 @default Jan.,Feb.,Mar.,Apr.,May,Jun,Jul,Aug.,Sept.,Oct.,Nov.,Dec.

 @param Days in a Month
 @desc Define the number of days in each month
 @default 31,28,31,30,31,30,31,31,30,31,30,31

 @param Year Suffix
 @desc The year suffix name(E.G. AD, BC, etc)
 @default A.D.

 @param Pause With Message
 @desc Pause time from moving when dialogue window is open.
 @default false

 @param Pause In Battle
 @desc Pause time from moving when in the battle scene.
 @default true

 @param Pause In Menu
 @desc Pause time from moving when main menu is open.
 @default true

 @param ================
 @param  Display Clock
 @param ================

 @param Toggle Button
 @desc The button to press to show and hide the clock.
 @default tab

 @param 24 Hour Time
 @desc Would you like to display the time in 24hour time.
 @default false

 @param Integrate On Map
 @desc Integrate the clock on the map.
 @default true

 @param Clock Detials For Map
 @desc Change the position of the clock on the map.
 @default x:right, y:top, width:230, format:compact, font:24

 @param Integrate In Menu
 @desc Integrate the clock in the main menu.
 @default false

 @param Clock Detials For Menu
 @desc Change the position of the clock in the main menu.
 @default x:left, y:420, width:230, format:compact, font:24

 @param Integrate In Battle
 @desc Integrate the clock in the battle scene
 @default false

 @param Clock Detials For Battle
 @desc Change the position of the clock in battle.
 @default x:right, y:top, width:230, format:compact font:24

 @param Active Variable Details
 @desc How should the active variables be displayed?
 @default format:standard



* @help
 ================================================================================
 ▼ TERMS OF USE
 ================================================================================
 Credit goes to: LTN Games

 Exclusive to rpgmakermv.co, please don't share anywhere else unless given strict
 permission by the author of the plugin.

 The plugin may be used in commerical and non-commerical products.
 Credit must be given!

 Please report all bugs to http://www.rpgmakermv.co/threads/time-engine.4886/

 ===============================================================================
 ▼ DEPENDENCIES
 ===============================================================================
 Requires LTN Core.

 Ensure you have LTN Core installed and above this plugin.

 ===============================================================================
 ▼ INFORMATION
 ===============================================================================
 This is the core time engine for a a series of plugins to give your game basic
 time and other time related features.

 This core gives you a complete basic time system and does not require any of
 the add-ons to work. With this plugin you will have access to a display clock
 (HUD) which can be shown on the game map, battle scene or main menu.
 ============================================================================
 ▼ DONATIONS
 ============================================================================
Most of the plugins I write are free to use commercially, and I put a lot of
hours into the development of my plugins, If you like my work and want to see
more of it in the future, I ask that you consider offering a donation.

If you do wish to provide your support, you can do so by sending your
contribution via PayPal to - LTNGamesDevelopment at gmail.com
 ===============================================================================
 ▼ INSTRUCTIONS
 ===============================================================================
By default when this plugin is turned on it will start working right away, you
will have a clock on the game map when starting a new game, set to the default
time. It is up to you to customize the details of how you want the clock to be
displayed and you can start with the plugins parameter setting.

===================
PlUGIN PARAMETERS:
===================
Most plugin parameters you can choose from are self explanatory, but for the
ones which are not described well, I'll explain here.

A quick note about setting up months, weeks, and days. Just keep note of how
the default values are formatted you always use a , after each month, day or
week.

The first name in the 'months' parameter will be 'January', so the first one
in the 'Abreviated Months' parameter must be 'Jan' and the first one in the
'days in a month' parameter will be be 31 and so on.

The first name in Months is also the first month of the year, when the time
has reached all the way the last month it will reset back to the first month.
You may include as many months as you'd like.

-----------------------------
The clock detail parameters
------------------------------
These parameters are for changing the appearance and location of the clock.

Clock Detials For Map
Clock Detials For Menu
Clock Detials For Battle

Each of these can be adjusted to your liking using a simple format like
this below.

 x:left, y:420, width:230, height:80, format:compact, font:24

 Just ensure you type the property you want to adjust followed by a : then
 the value you want that property to be, followed by a comma ,.

 x: Is the location of the window on the x axis(left and right) You can
 keep it simple and use the word left to align the clock to the 'left' or
 use the word 'right' to align the window to the right. You may also use
 numbers to fine tune and adjust the clock wherever you'd like on the x axis.

 y: Is the same as the x propperty but instead of left and right it's up
 and down. You may use the words bottom and top to align the window, and
 you may also use a number to fine tune and adjust the location.

 width: This is the width of the window, you really only need to adjust
 this if you have a larger default font, or you change the font size and
 notice the text is being cut off.

 format: This is the format of the date,
 'compact' will use all the abreviated names.
 'full' will use the full names of the month and weekday
 'simple' will remove the date entirely, and show just the clock,

 font: This is the font size of the date and time.

===================
PlUGIN COMMANDS:
===================

Plugin Command:
-----------------------------------------
TE SetTime hour minute day week month year
------------------------------------------
eg:
TE SetTime 18 30 14 Thursday December 2000
TE SetTime 18 30 14 5 12 2000
TE SetTime x x 14 x 12 x
TE SetTime 16 30

This plugin command will set the date and time if you choose to.

You do not have to enter a time, if you only want to set 1 option
simply type in an x on the option(s) you don't want to set
in the plugin command. As seen in the exampled above.

The hour option uses 24 hour time. It must be a number set between 1
and the amount you set in the parameter "Hours In A Day"

The minute option is a number between 1 and the amount you set in
the parameter "Minutes In an Hour".

The day option is the day of the month you want to switch to, ensure
you don't set the day past the max amount of days in a month.

The week option is the day of the week(the name or number) ensure you
don't set past the max amount of days in a week.

The Month option is to set the month in a year(the name or number)
ensure you don't  set past the max amount of months in a year.

The year option is, you guessed it, the year. You can set this to whatever
you want just ensure it's only a number.

Plugin Command:
------------------------------------------
TE SetSpeed Speed Seconds
------------------------------------------
eg:
TE SetSpeed 3, 5
TE SetSpeed 1, 60

The speed option is to fine tune the speed of the time. The higher the number
the slower the time will progress.

The seconds option is a number you may change to whatever you want
and represents how much seconds are in a minute. This will also speed up or
slow down time.

Plugin Command:
------------------------------------------
TE Pause
------------------------------------------
eg:
TE Pause true   //Pause time
TE Pause false  //Unpause time

This is for pausing the time during cutscenes or whenever you need to pause time.
You can use the same command to unpause time as well.
Note: You may use the keywords 'true', 'yes', 'false', and 'no' to pause and unpause.

Plugin Command:
------------------------------------------
TE Variables static/current Hour Minute Day Week Month Year
------------------------------------------
eg:
# Takes a snapshot of the time, which  will always remain the time when you set them.
TE Variables static 1 2 3 4 5 6

# This will always update the variables to be the current time.
TE Variables active 1 2 3 4 5 6

# This will clear the active variables from being updated
TE Variables clear

This plugin command will setup the game time to the variable # you set.
In the example above the hour will go into variable 1 and so on.

variables - is a keyword which lets the plugin know you want to set variables

static or active - the third keyword let's the plugin know you want to get a
timestamp(static) of the current time or you want the variabels to be always updated
(active).

clear - This is a third keyword you can use to clear the active variables, this will
stop the active variables from being updated, until you set the active variables again.
NOTE: The variables will still reflect the time it was when you cleared the active variables.

------------------------------------------
TE Clock hide
------------------------------------------
eg:
TE Clock hide

This plugin command will hide the clock from all scenes you intergrated it into
until you decide to show it again.
------------------------------------------
TE Clock show
------------------------------------------
eg:
TE Clock show

This plugin command will show the clock from all scenes you intergrated it into
until you decide to hide it again.


===================
MESSAGE CODES:
===================
\STIME  - this will retrieve the current time in standard time. eg: 5:00AM
\24TIME  - this will retireve the current time in army time.     eg: 14:35

\FDATE  - this will retrieve the current date in full format. eg: Sunday, January 1
\SDATE  - this will retrieve the current date in simple format. eg: Sun, Jan 1



 */


(function () {
	if (typeof LTN === 'undefined') {
		var strA = "You need to install the LTN Core plugin ";
		var strB = "in order for this plugin to work! Please visit";
		var strC = "\n http://www.rmmv.co/resources/authors/ltn-games.17/";
		var strD = " to download the latest verison.";
		throw new Error(strA + strB + strC + strD);
	} else {
		// Set plugin namespace
		var register = LTN.requireUtils(false, "registerPlugin");
		register('TimeEngine', '1.0.2', 'LTNGames');
	}
})();

(function ($) {
	'use strict';
	//Require Utility functions
	var $Utils = LTN.requireUtils(true);

	$.Parameters = PluginManager.getPluginParameters('LTN_TimeEngine');
	$.Param = $.Param || {};
	//Game Time
	$.Param.monthNames 		 = String($.Parameters['Month Names']);
	$.Param.monthNameAbvs    = String($.Parameters['Month Abreviations']);
	$.Param.monthDays 		 = String($.Parameters['Days in a Month']);

	$.Param.weekNames        = String($.Parameters['Days of the Week']);
	$.Param.weekNameAbvs     = String($.Parameters['Days of the Week Abreviations']);

	$.Param.yearSuffix 		 = String($.Parameters['Year Suffix']);
	$.Param.timeSpeed 		 = Number($.Parameters['Time Speed']);

	$.Param.secondsInMin 	 = Number($.Parameters['Seconds In A Minute']);
	$.Param.minutesInHour 	 = Number($.Parameters['Minutes In An Hour']);
	$.Param.hoursInDay   	 = Number($.Parameters['Hours In A Day']);

	$.Param.pauseInMenu 	 = $Utils.toBool($.Parameters['Pause In Menu']);
	$.Param.pauseInBattle    = $Utils.toBool($.Parameters['Pause In Battle']);
	$.Param.pauseWhenMessage = $Utils.toBool($.Parameters['Pause With Message']);

	$.Param.mapDetails       = $Utils.toObj($.Parameters['Clock Detials For Map']);
	$.Param.menuDetails      = $Utils.toObj($.Parameters['Clock Detials For Menu']);
	$.Param.battleDetails    = $Utils.toObj($.Parameters['Clock Detials For Battle']);

	$.Param.mapIntegrate     = $Utils.toBool($.Parameters['Integrate On Map']);
	$.Param.battleIntegrate  = $Utils.toBool($.Parameters['Integrate In Battle']);
	$.Param.menuIntegrate    = $Utils.toBool($.Parameters['Integrate In Menu']);

	$.Param.twentyFourHour   = $Utils.toBool($.Parameters['24 Hour Time']);
	$.Param.toggleButton     = String($.Parameters['Toggle Button']) || 'tab';

	$.Param.activeVarConfig  = $Utils.toObj($.Parameters['Active Variable Details']);

	/**----------------------------------------------------
	 * Add Clock To Scenes >>
	 *
	 * Aliased Methods: SceneMenu, SceneMap, SceneBattle
	 * SceneBase
	 -------------------------------------------------------*/

	$.Alias.SceneMap_createDisplayObjs = Scene_Map.prototype.createDisplayObjects;

	Scene_Map.prototype.createDisplayObjects = function () {
		$.Alias.SceneMap_createDisplayObjs.call(this);
		if($.Param.mapIntegrate) {$.gameTime.createClock();}
	};

	$.Alias.Scene_Menu_create = Scene_Menu.prototype.create;

	Scene_Menu.prototype.create = function () {
		$.Alias.Scene_Menu_create.call(this);
		if($.Param.menuIntegrate) {$.gameTime.createClock();}
	};

	$.Alias.SceneBattle_create = Scene_Battle.prototype.create;
	Scene_Battle.prototype.create = function () {
		$.Alias.SceneBattle_create.call(this);
		if($.Param.battleIntegrate) {$.gameTime.createClock();}
	};

	$.Alias.SceneBase_update = Scene_Base.prototype.update;

	Scene_Base.prototype.update = function () {
		$.Alias.SceneBase_update.call(this);
		$.gameTime.updateTime();
	};


	/**----------------------------------------------------
	 * Game_Time >>
	 *
	 * Basic time system
	 * Record time events, update time.
	 * Functions: Set speed, current hh:mm:ss, update, record
	 *
	 -------------------------------------------------------*/
	function Game_Time() {
		this.initialize.apply(this, arguments);
	}

	Game_Time.prototype.initialize = function () {
		var speed = $.Param.timeSpeed;
		this.time = {year: 2016, month: 1, day: 1, week: 1,
					 hour: 6, minute:30, second: 0,
					 millisecond: 0, speed: speed
					};
		this._pauseTime = false;
		this._clockHide = false;
		this._activeVars = [];
		this._activeVarInfo = $.Param.activeVarConfig;
		this.setupDatesInformation();
	};

	// Converts all parameters for months, days and weeks into an object of arrays.
	Game_Time.prototype.setupDatesInformation = function () {
		this.date = {days:     $Utils.toArray($.Param.monthDays),
					 months:   $Utils.toArray($.Param.monthNames),
					 monthsAbv:$Utils.toArray($.Param.monthNameAbvs),
					 weeks:    $Utils.toArray($.Param.weekNames),
					 weeksAbv: $Utils.toArray($.Param.weekNameAbvs)
					};
	};

	Game_Time.prototype.createClock = function () {
		this._winTimeHUD = new Window_TimeHUD();
		SceneManager._scene.addChild(this._winTimeHUD);
	};

	Game_Time.prototype.hideClock = function (option) {
		if(SceneManager._scene.constructor === Scene_Map){
			if(option === 'hide'){
				this._winTimeHUD.close();
				this._clockHide = true;
			} else if(option === 'show') {
				this._winTimeHUD.open();
				this._clockHide = false;
			}
		}
	};

	Game_Time.prototype.pauseTime = function (bool) {
		this._pauseTime = bool;
	};

	Game_Time.prototype.currentTime = function () {
		return this.time;
	};

	Game_Time.prototype.currentYear = function () {
		return this.time.year;
	};

	Game_Time.prototype.currentDate = function () {
		var date = {month : this.currentMonth(),
					week  : this.currentWeek(),
					day   : this.time.day,
					year  : this.time.year
				   };
		return date;
	};

	// A function to retrieve the current weeks full details.
	Game_Time.prototype.currentWeek = function () {
		var week = {};
		week.name    = this.date.weeks[this.time.week - 1];
		week.nameAbv = this.date.weeksAbv[this.time.week - 1];
		week.number  = this.date.weeks.indexOf(week.name) + 1;
		return week;
	};

	// A function to retrieve the current months full detials.
	Game_Time.prototype.currentMonth = function () {
		var month = {};
		month.name    = this.date.months[this.time.month - 1];
		month.nameAbv = this.date.monthsAbv[this.time.month - 1];
		month.number  = this.date.months.indexOf(month.name) + 1;
		month.days    = parseInt(this.date.days[month.number - 1]);
		return month;
	};

	Game_Time.prototype.setSpeed = function (speed) {
		this.time.speed = speed;
	};

	Game_Time.prototype.setTime = function (hour, minute) {
		this.time.hour   = hour;
		this.time.minute = minute;
	};

	Game_Time.prototype.setDate = function (day, week, month, year) {
		this.time.day   = day;
		this.time.week  = !isNaN(week)  ? week  : this.date.weeks.indexOf(week) +1;
		this.time.month = !isNaN(month) ? month : this.date.months.indexOf(month) +1;
		this.time.year  = year;
	};

	Game_Time.prototype.clearActiveVariables = function (){
		this._activeVars.length = 0;
	};

	Game_Time.prototype.updateActiveVariables = function () {
		if(this._activeVars.length < 0){return;}
		var time = this.time;
		var times = [time.hour, time.minute, time.day, this.currentWeek().name, this.currentMonth().name, time.year];
		var varsMax = this._activeVars.length;
		for(var i = 0; i < varsMax; i++){
			if(!isNaN(this._activeVars[i]) || this._activeVars[i] !== 'x'){
				$gameVariables.setValue(this._activeVars[i], times[i]);
			}
		}
	};

	Game_Time.prototype.setStaticVariables = function (hour, minute, day, week, month, year) {
		var time = this.time;
		var times = [time.hour, time.minute, time.day, this.currentWeek().name, this.currentMonth().name, time.year];
		var variables = [Number(hour), Number(minute), Number(day), Number(week), Number(month), Number(year)];
		var varMax = variables.length;
		for(var i = 0; i < varMax; i++){
			if(!isNaN(variables[i]) || variables[i] !== 'x'){
				$gameVariables.setValue(variables[i], times[i]);
			}
		}
	};

	//----------------------------------------------------------
	// The main time function, updates seconds, minutes & hours.
	// Call in Scene update functions
	//----------------------------------------------------------

	Game_Time.prototype.updateTime = function () {
		if(this._pauseTime){return;}
		if($.Param.pauseInMenu && SceneManager._scene instanceof Scene_MenuBase) {return;}
		if($.Param.pauseInBattle && SceneManager._scene.constructor === Scene_Battle) {return;}
		if($.Param.pauseWhenMessage && $gameMessage.isBusy()) {return;}
		this.updateActiveVariables();
		this.addSeconds();
	};

	Game_Time.prototype.addSeconds = function () {
		this.time.millisecond++;
		// Millsecond To Second
		if (this.time.millisecond >= this.time.speed) {
			this.time.second++;
			this.addMinute();
			this.time.millisecond = 0;
		}
	};

	Game_Time.prototype.addMinute = function () {
		// Second To Minute
		if (this.time.second >= $.Param.secondsInMin) {
			this.time.minute++;
			this.addHour();
			this.time.second = 0;
		}
	};

	Game_Time.prototype.addHour = function () {
		// Minute To Hour
		if (this.time.minute >= $.Param.minutesInHour) {
			if (this.time.hour >= $.Param.hoursInDay) {
				this.time.hour = 1;
				this.time.minute = 0;
			} else {
				this.time.hour++;
				this.time.minute = 0;
			}
			// If Midnight Add a day
			if(this.time.hour >= $.Param.hoursInDay && this.time.minute === 0){
				this.addDay();
			}
		}
	};

	Game_Time.prototype.addDay = function () {
		// If last day of month, add month else add day
		if (this.time.day == this.currentMonth().days) {
			this.addMonth();
			this.time.day = 1;
		} else {
			this.time.day++;
		}
		this.addWeek();
	};

	Game_Time.prototype.addWeek = function () {
		if(this.time.week >= this.date.weeks.length){
			this.time.week = 1;
		} else {
			this.time.week++;
		}
	};

	Game_Time.prototype.addMonth = function () {
		if (this.currentMonth().number === this.date.months.length) {
			this.addYear();
			this.time.month = 1;
		} else {
			this.time.month++;
		}
	};

	Game_Time.prototype.addYear = function () {
		this.time.year++;
	};


	/**----------------------------------------------------
	 * Window_Base >>
	 * Extra escape charcters for displaying time and date in messages.
	 -------------------------------------------------------*/
	$.Alias.Window_Base_convertEscapeCharacter = Window_Base.prototype.convertEscapeCharacters;

	Window_Base.prototype.convertEscapeCharacters = function(text) {
		text = $.Alias.Window_Base_convertEscapeCharacter.call(this, text);
		text = text.replace(/\STIME/g, function () {
			return String($Utils.toTimeFormat(0, 0, $.gameTime.time));
		}.bind(this));

		text = text.replace(/\24TIME/g, function () {
			return String($Utils.toArmyTime(0, 0, $.gameTime.time));
		}.bind(this));

		text = text.replace(/\FDATE/g, function () {
			var date = $.gameTime.currentDate();
			var week  = String(date.week.name);
			var month = String(date.month.name);
			var day   = $.gameTime.time.day;
			var fdate = ' ' + week + ', ' + month + ' ' + day;
			return fdate;
		}.bind(this));

		text = text.replace(/\SDATE/g, function () {
			var date = $.gameTime.currentDate();
			var week  = String(date.week.nameAbv);
			var month = String(date.month.nameAbv);
			var day   = $.gameTime.time.day;
			var sdate = ' ' + week + ', ' + month + ' ' + day;

			return sdate;
		}.bind(this));

		return text;
	};
	/**----------------------------------------------------
	 * Window_TimeHUD >>
	 *
	 * Main time window shown on Scene_Map
	 * Display hh:mm:ss or army time HH:MM
	 -------------------------------------------------------*/


	function Window_TimeHUD() {
		this.initialize.apply(this, arguments);
	}

	Window_TimeHUD.prototype = Object.create(Window_Base.prototype);
	Window_TimeHUD.prototype.constructor = Window_TimeHUD;
	//-------=====---------
	//Initialize
	//-------=====---------
	Window_TimeHUD.prototype.initialize = function () {
		this._info = this.currentSceneInfo();
		this._format = this._info.format ? this._info.format : 'compact';
		var w = this.windowWidth();
		var h = this.windowHeight();
		Window_Base.prototype.initialize.call(this, 0, 0, w, h);

		this.padding = 5;
		// Assign x & y position after base call.
		this.x = this.windowX();
		this.y = this.windowY();
		// Assign Global Time to local time vars.
		this._time = $.gameTime.currentTime();
		this.visible = $.gameTime._clockHide ? false : true;
		this.refresh();
	};

	Window_TimeHUD.prototype.update = function () {
		Window_Base.prototype.update.call(this);
		this.toggleButton();
		this.refresh();
	};

	Window_TimeHUD.prototype.refresh = function () {
		this.contents.clear();

		if (this._format === 'full' || this._format === 'compact') {
			this.contents.fontSize = this._info.font;
			this.drawTime(this._time, 0, 35);
			this.drawDate(0, 0);
		}

		if (this._format === 'simple') {
			this.contents.fontSize = this._info.font;
			this.drawTime(this._time, 0, 0);
		}
	};

	Window_TimeHUD.prototype.drawDate = function (x, y) {
		var date = $.gameTime.currentDate();
		var year = date.year;
		var month = this._format !== 'compact' ? date.month.name : date.month.nameAbv;
		var week = this._format  !== 'compact' ? date.week.name : date.week.nameAbv;
		var day = date.day;
		var yearSuffix = ' ' + $.Param.yearSuffix;
		if (this._format === 'compact') {
			this.drawText(week + ' ' + month + ' ' + day + ', ' + year, x, y, this.width, 'center');
		} else {
			this.drawText(week + ', ' + month + ' ' + day + ', ' + year + yearSuffix, x, y, this.width, 'center');
		}
	};

	Window_TimeHUD.prototype.drawTime = function (time, x, y) {
		var text = $.Param.twentyFourHour ? $Utils.toArmyTime(0, 0, time) : $Utils.toTimeFormat(0, 0, time);
		this.drawText(text, x, y, this.width, 'center');
	};

	Window_TimeHUD.prototype.toggleButton = function () {
		if(SceneManager._scene.constructor === Scene_Map && $.gameTime._clockHide === false){
			if(Input.isTriggered($.Param.toggleButton) && !this.visible){
				this.open();
			} else if(Input.isTriggered($.Param.toggleButton) && this.visible) {
				this.close();
			}
		}
	};

	/**----------------------------------------------------
		* Window Appearance Functions >>
	-------------------------------------------------------*/
	Window_TimeHUD.prototype.currentSceneInfo = function () {
		switch(SceneManager._scene.constructor) {
			case Scene_Map:
				return $.Param.mapDetails;
			case Scene_Menu:
				return $.Param.menuDetails;
			case Scene_Battle:
				return $.Param.battleDetails;
			default:
				return $.Param.mapDetails;
		}
	};

	Window_TimeHUD.prototype.windowX = function () {
		return (this._info.x === 'right') ? (Graphics.width - this.width) :
		(this._info.x === 'left')  ? 0 : this._info.x ? Number(this._info.x) : 0;
	};

	Window_TimeHUD.prototype.windowY = function () {
		return (this._info.y  === 'bottom') ? Graphics.height - this.height :
		(this._info.y === 'top')    ? 0 : this._info.y ? Number(this._info.y) : 0;
	};

	Window_TimeHUD.prototype.windowWidth = function () {
		return this._info.width ? Number(this._info.width) : this._format === 'simple' ? 100 : 280;
	};

	Window_TimeHUD.prototype.windowHeight = function () {
		return this._info.height ? Number(this._info.height) : this._format === 'simple' ? 50 : 80;
	};

	Window_TimeHUD.prototype.standardPadding = function () {
		return 5;
	};

	Window_TimeHUD.prototype.close = function () {
		this.visible = false;
	};

	Window_TimeHUD.prototype.open = function () {
		this.visible = true;
	};

	/**----------------------------------------------------
	 * Plugin Commands >>
	 *
	 * Contain time engine plugin commands.
	 *
	 -------------------------------------------------------*/
	$.Alias.GameInterp_pluginCommand = Game_Interpreter.prototype.pluginCommand;
	Game_Interpreter.prototype.pluginCommand = function (command, args) {
		if (command === 'TE') {
			var time = $.gameTime.currentTime();
			switch (args[0].toLowerCase()) {
					// TC SETTIME Hour Minute
				case 'settime':
					var hour   = args[1] !== 'x' ? Number(args[1]) : time.hour;
					var minute = args[2] !== 'x' ? Number(args[2]) : time.minute;
					var day    = args[3] !== 'x' ? Number(args[3]) : time.day;
					var week   = args[4] !== 'x' ? args[4] : time.week;
					var month  = args[5] !== 'x' ? args[5] : time.month;
					var year   = args[6] !== 'x' ? Number(args[6]) : time.year;
					$.gameTime.setTime(hour, minute);
					if (args.length > 2) {
						$.gameTime.setDate(day, week, month, year);
					}
					break;

				case 'setspeed':
					var speed = args[1] ? Number(args[1]) : time.speed;
					var secondIn = args[2] ? Number(args[2]) : time.second;
					$.gameTime.setSpeed(speed, secondIn);
					break;

				case 'pause':
					var pause = args[1] ? $Utils.toBool(args[1]) : false;
					$.gameTime.pauseTime(pause);
					break;

					// TE variables hour minute day week month year
				case 'variables':
					// Set variables to make a timstamp of current time.
					if(args[1].toLowerCase() === 'static'){
						$.gameTime.setStaticVariables(args[2], args[3], args[4], args[5], args[6], args[7]);
					}
					// Set active variables which always update
					else if(args[1].toLowerCase() === 'active'){
						var varArgs = [args[2], args[3], args[4], args[5], args[6], args[7]];
						var argsMax = varArgs.length;
						for(var i = 0; i < argsMax; i++){
							$.gameTime._activeVars.push(varArgs[i]);
						}
					}
					// Clear active variables from updating.
					else if(args[1].toLowerCase() === 'clear'){
						$.gameTime.clearActiveVariables();
					}

					break;

				case 'clock':
					$.gameTime.hideClock(args[1]);
					break;
			}
		} else {
			$.Alias.GameInterp_pluginCommand.call(this, command, args);
		}
	};

	/**----------------------------------------------------
	 * EXPORT FOR ADD-ONS>>
	 -------------------------------------------------------*/
	$.Game_Time    = Game_Time;
	$.gameTime     = new Game_Time();
	$.Window_Clock = Window_TimeHUD;

	/**----------------------------------------------------
	 * OBJECT(s) TO SAVE WITH GAME
	 -------------------------------------------------------*/
	var saveContents = [$.gameTime.time, $.gameTime._activeVars, $.gameTime._paused, $.gameTime._clockHide];
	DataManager.addToSaveContents('gameTime', saveContents);


}(LTN.requirePlugins(false, 'TimeEngine')));
/**----------------------------------------------------
 * END OF PLUGIN, TRUE STORY!>>
-------------------------------------------------------*/
