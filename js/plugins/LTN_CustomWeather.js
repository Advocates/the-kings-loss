//-----------------------------------------------------------------------------
//LTN_CustomWeather.js
//-----------------------------------------------------------------------------
// Version 1.0
/*:
 @plugindesc v1.0 A simple custom weather plugin, designed to change defualt weather color or use a bitmap.

 @author LTN Games

 @param --Defualt Weather--

 @param Overwrite Defualt
 @desc For compatability reaosns you can choose to keep the defualt settings.
 @default true

 @param Rain Color
 @desc Change the defualt color of the rain.
 @default white

 @param Storm Color
 @desc Change the defualt color of the rain for a storm.
 @default white

 @param Snow Color
 @desc Change the defualt color of the snow.
 @default white

 @param Rain Bitmap
 @desc Replace defualt color with a bitmap instead.
 @default RainDrop

 @param Storm Bitmap
 @desc Replace defualt color with a bitmap instead.
 @default RainDrop

 @param Snow Bitmap
 @desc Replace defualt color with a bitmap instead.
 @default SnowFlake

 @param -- Custom Weather --
 @param Blizzard Bitmap
 @desc Change the bitmap of the rain effect to your own custom graphic.
 @default SnowFlake

 @param Bubbles Bitmap
 @desc Change the bitmap of the rain effect to your own custom graphic.
 @default Bubble_Green

 @param Light Orb Bitmap
 @desc Change the bitmap of storm effect to your own custom graphic.
 @default LightOrb


 @help
------------------------------------------------------------------------------
 A simple custom weather plugin which allows you to choose images for your
 weather instead of the defualt drawn shapes. I kept it as simple as possible
 to try and keep the original weather system effects intact.
 The defualt weather effects can be manipulated to your choosing, you can
 use a bitmap or a color as well as change the defualt colors.

 For compatability reasons I also allow the option to
 disable overwriting of the defualt weather effects and the plugin will call the
 original method.

 Disabling the overwriting of the defualt weather does not effect the custom
 weather in the plugin including 'bubbles', 'blizzard, & 'orbs'.

 If the bitmap option in the plugin manager is empty it will defualt to the color.
 All custom weather effects must have a bitmap enabled in the plugin manager.

 PLUGIN Command
 CUSTOMWEATHER EffectName EffectPower EffectDuration

 Effect Name    = rain, snow, storm, bubbles, orbs, or blizzard.
 Effect Power   = The power of the weather, defualt from 0-9
 Effect Duration = The duration before procceding to  the full effect of weather.
------------------------------------------------------------------------------
*/
//=============================================================================
// Plugin Parameters
//=============================================================================

LTN = LTN || {};
LTN.CustomWeather = LTN.CustomWeather || {};
(function($) {
$.Parameters = PluginManager.parameters('LTN_CustomWeather');
$.Param = $.Param || {};
$.Param.reWriteDef       = String($.Parameters['Overwrite Defualt']).toLowerCase();
$.Param.rainColor        = String($.Parameters['Rain Color'].toLowerCase());
$.Param.stormColor       = String($.Parameters['Storm Color'].toLowerCase());
$.Param.snowColor        = String($.Parameters['Snow Color'].toLowerCase());
$.Param.rainBitmap       = String($.Parameters['Rain Bitmap']);
$.Param.stormBitmap      = String($.Parameters['Storm Bitmap']);
$.Param.snowBitmap       = String($.Parameters['Snow Bitmap']);
$.Param.blizzardbBitmap  = String($.Parameters['Blizzard Bitmap']);
$.Param.bubblesBitmap    = String($.Parameters['Bubbles Bitmap']);
$.Param.lightOrbBitmap   = String($.Parameters['Light Orb Bitmap']);
//=============================================================================
// Global Plugin Variables
//=============================================================================


//=============================================================================
// Weather
//=============================================================================
// Aliased Methods :
$.sm_oldWeather_createBitmaps = Weather.prototype._createBitmaps;
$.sm_oldWeather_updateSprite  = Weather.prototype._updateSprite;
//=============================================================================
//--------------
//OVERWRITE: Create Bitmaps
//--------------
Weather.prototype._createBitmaps = function() {
  if($.Param.reWriteDef === 'true'){
    //-----Rain-------------
    if($.Param.rainBitmap){
      this._rainBitmap = ImageManager.loadPicture($.Param.rainBitmap);
    } else {
      this._rainBitmap = new Bitmap(1, 60);
      this._rainBitmap.fillAll($.Param.rainColor);
    }
    //-----Storm-------------
    if($.Param.stormBitmap){
      this._stormBitmap = ImageManager.loadPicture($.Param.stormBitmap);
    } else {
      this._stormBitmap = new Bitmap(2, 100);
      this._stormBitmap.fillAll($.Param.stormColor);
    }
    //-----Snow-------------
    if($.Param.snowBitmap){
      this._snowBitmap = ImageManager.loadPicture($.Param.snowBitmap);
    } else {
      this._snowBitmap = new Bitmap(9, 9);
      this._snowBitmap.drawCircle(4, 4, 4, $.Param.snowColor);
    }
  } else {
    $.sm_oldWeather_createBitmaps.call(this);
  }
  //--
  this._blizzardBitmap = new Bitmap(9, 9);
  this._blizzardBitmap = ImageManager.loadPicture($.Param.blizzardbBitmap);
  //--
  this._bubblesBitmap = new Bitmap(9, 9);
  this._bubblesBitmap = ImageManager.loadPicture($.Param.bubblesBitmap);
  //--
  this._orbsBitmap = new Bitmap(9, 9);
  this._orbsBitmap = ImageManager.loadPicture($.Param.lightOrbBitmap);
};
//--------------
//OVERWRITE: Weather Update
//--------------
Weather.prototype._updateSprite = function(sprite) {
  switch (this.type) {
    case 'rain':
    this._updateRainSprite(sprite);
    break;
    case 'storm':
    this._updateStormSprite(sprite);
    break;
    case 'snow':
    this._updateSnowSprite(sprite);
    break;
    case 'blizzard':
    this._updateBlizzard(sprite);
    break;
    case 'bubbles':
    this._updateBubbles(sprite);
    break;
    case 'orbs':
    this._updateLightOrbs(sprite);
    break;
  }
  if (sprite.opacity < 40) {
    this._rebornSprite(sprite);
  }
};
//-----------------------------------------------------------------------------
//Custom Weather Update methods
//-----------------------------------------------------------------------------
Weather.prototype._updateBlizzard = function(sprite) {
    sprite.bitmap = this._blizzardBitmap;
    sprite.rotation = Math.PI / 8;
    sprite.ax -= 4 * Math.sin(sprite.rotation);
    sprite.ay += 6 * Math.cos(sprite.rotation);
    sprite.opacity -= 3;
};
Weather.prototype._updateBubbles = function(sprite) {
  sprite.bitmap = this._bubblesBitmap;
  sprite.rotation = Math.PI / 16;
  sprite.ax += 3 * Math.sin(sprite.rotation);
  sprite.ay -= 3 * Math.cos(sprite.rotation);
  sprite.opacity -= 3;
};
Weather.prototype._updateLightOrbs = function(sprite) {
  sprite.bitmap = this._orbsBitmap;
  sprite.rotation = Math.PI / 16;
  sprite.ax += 6 * Math.sin(sprite.rotation);
  sprite.ay -= 2 * Math.cos(sprite.rotation);
  sprite.opacity -= 5;
};
//=============================================================================
// Game Interperter Class : For Plugin Command
$.CW_oldGameInterp_pluginCommand = Game_Interpreter.prototype.pluginCommand;
//=============================================================================
Game_Interpreter.prototype.pluginCommand = function(command, args) {
  $.CW_oldGameInterp_pluginCommand.call(this, command, args);
  if(command === 'CUSTOMWEATHER'){
    var effectName     = args[0] ? String(args[0]) : '';
    var effectPower    = args[1] ? Number(args[1]) : 0;
    var effectDuration = args[2] ? Number(args[2]) : 0;
    $gameScreen.changeWeather(effectName, effectPower, effectDuration);
  }
};
}(LTN.CustomWeather));
//=============================================================================
// The End, based on a true story :D
//=============================================================================
