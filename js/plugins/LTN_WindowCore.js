//=============================================================================
//LTN_ScriptName.js
//=============================================================================
// Version 1.0
/*:
 @plugindesc V1.0 Window core plugin which changes many elements of windows.
 <LTN_WindowCore>

 @author LTN Games

 @param ================
 @param  Window Options
 @param ================

 @param Window Opacity
 @desc Adjust the all windows opacity
 Default: 198
 @default 198

 @param Icon Dimensions
 @desc Adjust the dimensions of the icons.
 Default: width:32, height:32
 @default width:32, height:32

 @param Face Dimensions
 @desc Adjust the dimensions of the face image.
 Default: width:144, height:144
 @default width:144, height:144

 @param TP In Menu
 @desc Includes the TP Gauge in the menu.
 Default: true
 @default true

 @param ================
 @param  Font Options
 @param ================

 @param Default Font
 @desc Adjust the defualt font for your game.
 Default: GameFont
 @default GameFont

 @param Default Chinese Font
 @desc Adjust the defualt font for your game.
 Default: SimHei, Heiti TC, sans-serif
 @default SimHei, Heiti TC, sans-serif

 @param Default Korean Font
 @desc Adjust the defualt font for your game.
 Default: Dotum, AppleGothic, sans-serif
 @default Dotum, AppleGothic, sans-serif

 @param Font Size
 @desc Adjust the defualt font size for your game.
 Default: 28
 @default 28

 @param Font Outline Size
 @desc Adjust the defualt font color for your game.
 Default: 4
 @default 4

 @param Font Outline Color
 @desc Adjust the defualt font color for your game.
 Default: 0, 0, 0, 0.5  - rgba value
 @default 0, 0, 0, 0.5

 @param ================
 @param  Color Options
 @param ================

 @param Font Color Options
 @desc The default color for all text in the game.
 Default: normal:0, system:16
 @default normal:0, system:16

 @param Crisis Colors
 @desc The default colors for death and crisis.
 Default: crisis:18, death:17
 @default crisis:18, death:17

 @param Cost Colors
 @desc The default colors for death and crisis.
 Default: mp:18, tp:17
 @default mp:18, tp:17

 @param Power Up/Down Colors
 @desc The default colors for power up and power down.
 Default: up:24, down:25
 @default up:24, down:25

 @param ================
 @param  Gauge Options
 @param ================

 @param Trim Gauge Width
 @desc Trim the width of the gauges on main and skill menu.
 Default: 100
 @default 100

 @param Gauges Height
 @desc The height of gauges.
 Default: 7
 @default 7

 @param Gauge Outlines
 @desc Enable gauges to have an outline
 Default: true
 @default true

 @param Gauge Outlines Color
 @desc Enable gauges to have an outline
 Default: 14
 @default 14

 @param Gauge Back Color
 @desc The default color for gauge back.
 Default: 19
 @default 19

 @param HP Gauge Colors
 @desc The default colors for the HP gauge.
 Default: color1:20, color2:21
 @default color1:20, color2:21

 @param MP Gauge Colors
 @desc The default colors for the MP gauge.
 Default: color1:22, color2:23
 @default color1:22, color2:23

 @param TP Gauge Colors
 @desc The default colors for the TP gauge.
 Default: color1:28, color2:29
 @default color1:28, color2:29

@help
================================================================================
▼ TERMS OF USE
================================================================================
Credit goes to: LTN Games

Exclusive to rpgmakermv.co, please don't share anywhere else unless given strict
permission by the author of the plugin.

The plugin may be used in commerical and non-commerical products.
Credit must be given!

Please report all bugs to http://www.rpgmakermv.co/threads/ScriptName

===============================================================================
▼ DEPENDENCIES
===============================================================================
Requires LTN Core.

===============================================================================
▼ INFORMATION
===============================================================================

===============================================================================
▼ INSTRUCTIONS
===============================================================================

*/

(function () {
	if (typeof LTN === 'undefined') {
		var strA = "You need to install the LTN Core ";
		var strB = "in order for this plugin to work! Please visit";
		var strC = "\n http://www.rmmv.co/resources/authors/ltn-games.17/";
		var strD = " to download the latest verison.";
		throw new Error(strA + strB + strC + strD);
	} else {
		// Set plugin namespace
		var register = LTN.requireUtils(false, "registerPlugin");
		register('WindowCore', '0.3.5', 'LTNGames');
	}
})();



(function ($) {
	'use strict';
	/*Create common utlity object from LTNCore*/
	var $Utils = LTN.requireUtils(true);

	/*Create an alias object for compatability and access to inner aliased functions
	 * Access with LTN.Plugins.WindowCore.Alias.aliasedMethod
	 * OR LTN.requirePlugins(false, 'WindowCore').Alias.aliasMethod;
	*/
	$.Alias = $.Alias || {};

	/*Parameter Setup*/
	$.Parameters = PluginManager.getPluginParameters('LTN_WindowCore');
	$.Param = $.Param || {};

	/*Window Settings*/
	$.Param.windowMax          = $Utils.toObj($.Parameters['Max Width & Height']);
	$.Param.msgWinOpacity      = Number($.Parameters['Message Window Opacity'] || 198);
	$.Param.iconSize      	   = $Utils.toObj($.Parameters['Icon Dimensions']);
	$.Param.faceSize      	   = $Utils.toObj($.Parameters['Face Dimensions']);
	$.Param.menuTpGauge        = $Utils.toBool($.Parameters['TP In Menu']);

	/*Font Settings*/
	$.Param.defaultChineseFont = String($.Parameters['Default Chinese Font']);
	$.Param.defaultKoreanFont  = String($.Parameters['Default Korean Font']);
	$.Param.defaultFont       = String($.Parameters['Default Font'] || 'GameFont');
	$.Param.fontColor 	       = String($.Parameters['Font Color'] || '#ffffff');
	$.Param.fontSize 	       = Number($.Parameters['Font Size'] || 28);
	$.Param.outlineSize        = Number($.Parameters['Font Outline Size'] || 4);
	$.Param.outlineColor       = String($.Parameters['Font Outline Color'] || 4);

	/*Font Color Setting*/
	$.Param.fontColors         = $Utils.toObj($.Parameters['Font Color Options']);
	$.Param.fontColorX         = $Utils.toObj($.Parameters['Font Color X Options']);

	/*Random Color Settings*/
	$.Param.crisisColors       = $Utils.toObj($.Parameters['Crisis Colors']);
	$.Param.costColors         = $Utils.toObj($.Parameters['Cost Colors']);
	$.Param.powerColors        = $Utils.toObj($.Parameters['Power Up/Down Colors']);

	/*Gauge Sttings*/
	$.Param.gaugesWidthTrim    = Number($.Parameters['Trim Gauge Width']);
	$.Param.gaugeBackColor     = Number($.Parameters['Gauge Back Color']);
	$.Param.hpGuageColors      = $Utils.toObj($.Parameters['HP Gauge Colors']);
	$.Param.mpGuageColors      = $Utils.toObj($.Parameters['MP Gauge Colors']);
	$.Param.tpGuageColors      = $Utils.toObj($.Parameters['TP Gauge Colors']);
	$.Param.gaugeHeight        = Number($.Parameters['Gauges Height']);
	$.Param.gaugeOutline       = $Utils.toBool($.Parameters['Gauge Outlines']);
	$.Param.gaugeOutlineColor  = Number($.Parameters['Gauge Outlines Color']);



	/*Create Aliad Object For Compatability Reasons*/

	/**-----------------------------------------------------------------------
     * Bitmap >>
     *
     *
     ------------------------------------------------------------------------*/
	/**
     * @function
     * @desc This closure function adds extra functionality to the Bitmap class
     */
	(function ($, Param) {
		var alias_Bitmap_intialize = $.initialize;

		$.initialize = function () {
			alias_Bitmap_intialize.apply(this, arguments);

			this._fontFace = Param.defaultFont;

			// Change default font outline to Parameter set outline?
			this.outlineWidth = Param.outlineSize;

			// Change default font outline to Parameter set outline?
			this.outlineColor = 'rgba(' + Param.outlineColor + ')';
		};

	})(Bitmap.prototype, $.Param);

	/**-----------------------------------------------------------------------
     * Window_Base >>
     *
     *
     ------------------------------------------------------------------------*/
	/**
     * @function
     * @desc This closure function extends and overwrites the Window_Base class.
     */


	(function ($, Param){

		Window_Base._iconWidth  = Param.iconSize.width  || 32;
		Window_Base._iconHeight = Param.iconSize.height || 32;
		Window_Base._faceWidth  = Param.faceSize.width  || 144;
		Window_Base._faceHeight = Param.faceSize.width  || 144;


		/**-----------------------------------------------------------------------
		* Font & Color Base Functions >>
		*
		------------------------------------------------------------------------*/
		$.standardFontFace = function() {
			if ($gameSystem.isChinese()) {
				return Param.defaultLocaleFont;
			} else if ($gameSystem.isKorean()) {
				return Param.defaultKoreanFont;
			} else {
				return Param.defaultFont;
			}
		};

		$.standardPadding = function() {
			return 18;
		};

		$.standardFontSize = function() {
			return Param.fontSize;
		};

		$.standardBackOpacity = function() {
			return Param.msgWinOpacity;
		};

		$.normalColor = function() {
			return this.textColor(Param.fontColors.normal) || this.textColor(0);
		};


		$.systemColor = function() {
			return this.textColor(Param.fontColors.system )  || this.textColor(16);
		};

		$.crisisColor = function() {
			return this.textColor(Param.crisisColors.crisis) || this.textColor(17);
		};

		$.deathColor = function() {
			return this.textColor(Param.crisisColors.death ) || this.textColor(18);
		};

		$.gaugeBackColor = function() {
			return this.textColor(Param.gaugeBackColor) || this.textColor(19);
		};

		$.hpGaugeColor1 = function() {
			return this.textColor(Param.hpGuageColors.color1) || this.textColor(20);
		};

		$.hpGaugeColor2 = function() {
			return this.textColor(Param.hpGuageColors.color2) || this.textColor(21);
		};

		$.mpGaugeColor1 = function() {
			return this.textColor(Param.mpGuageColors.color1) || this.textColor(22);
		};

		$.mpGaugeColor2 = function() {
			return this.textColor(Param.mpGuageColors.color2) || this.textColor(23);
		};

		$.mpCostColor = function() {
			return this.textColor(Param.costColors.mp) || this.textColor(23);
		};

		$.powerUpColor = function() {
			return this.textColor(Param.powerColors.up) || this.textColor(24);
		};

		$.powerDownColor = function() {
			return this.textColor(Param.powerColors.down) || this.textColor(25);
		};

		$.tpGaugeColor1 = function() {
			return this.textColor(Param.tpGuageColors.color1) || this.textColor(28);
		};

		$.tpGaugeColor2 = function() {
			return this.textColor(Param.tpGuageColors.color2) || this.textColor(29);
		};

		$.tpCostColor = function() {
			return this.textColor(Param.costColors.tp) || this.textColor(29);
		};

		$.gaugeOutlineColor = function() {
			return  this.textColor(Param.gaugeOutlineColor) || this.textColor(14);
		};

		/**-----------------------------------------------------------------------
		* Drawing Base Functions >>
		*
		------------------------------------------------------------------------*/
		$.gaugeHeight = function() {
			return Param.gaugeHeight || 6;
		};

		/*Overwrite Draw Gauge*/
		$.drawGauge = function(x, y, width, rate, color1, color2) {
			var fillW = Math.floor(width * rate);
			var gaugeH = this.gaugeHeight();
			var gaugeY = y + this.lineHeight() - gaugeH;
			if(Param.gaugeOutline){
				this.drawGaugeOutline(x, gaugeY, width, gaugeH);
				fillW = fillW - 4;
				x = x + 2;
			} else {
				this.contents.fillRect(x, gaugeY, width, gaugeH, this.gaugeBackColor());
			}
			this.contents.gradientFillRect(x, gaugeY, fillW, gaugeH, color1, color2);
		};

		/*NEW drawGaugeOutline*/
		$.drawGaugeOutline = function (x, gaugeY, width, gaugeH) {
			var color = this.gaugeOutlineColor();
			this.contents.paintOpacity = this.translucentOpacity();
			this.contents.fillRect(x, gaugeY - 2, width, gaugeH + 4, color);
			this.contents.paintOpacity = 255;
			this.contents.fillRect(x + 2, gaugeY, width - 4, gaugeH, this.gaugeBackColor());
		};

		$.drawActorHp = function(actor, x, y, width) {
			if(this.isHideGauge(actor, 'hidehp')){return;}
			width = width || 186;
			var color1 = this.hpGaugeColor1();
			var color2 = this.hpGaugeColor2();
			this.drawGauge(x, y, width, actor.hpRate(), color1, color2);
			this.changeTextColor(this.systemColor());
			this.drawText(TextManager.hpA, x, y, 44);
			this.drawCurrentAndMax(actor.hp, actor.mhp, x, y, width,
								   this.hpColor(actor), this.normalColor());
		};
		
		$.drawActorMp = function(actor, x, y, width) {
			if(this.isHideGauge(actor, 'hidemp')){return;}
			width = width || 186;
			var color1 = this.mpGaugeColor1();
			var color2 = this.mpGaugeColor2();
			this.drawGauge(x, y, width, actor.mpRate(), color1, color2);
			this.changeTextColor(this.systemColor());
			this.drawText(TextManager.mpA, x, y, 44);
			this.drawCurrentAndMax(actor.mp, actor.mmp, x, y, width,
								   this.mpColor(actor), this.normalColor());
		};
		
		$.drawActorTp = function(actor, x, y, width) {
			if(this.isHideGauge(actor, 'hidetp')){return;}
			width = width || 96;
			var color1 = this.tpGaugeColor1();
			var color2 = this.tpGaugeColor2();
			this.drawGauge(x, y, width, actor.tpRate(), color1, color2);
			this.changeTextColor(this.systemColor());
			this.drawText(TextManager.tpA, x, y, 44);
			this.changeTextColor(this.tpColor(actor));
			this.drawText(actor.tp, x + width - 64, y, 64, 'right');
		};
		//
		//$.drawActorLevel = function(actor, x, y) {
		//	this.changeTextColor(this.systemColor());
		//	this.drawText(TextManager.levelA, x, y, 48);
		//	this.resetTextColor();
		//	this.drawText(actor.level, x + 84, y, 36, 'right');
		//};

		/*Overwrite Simple Status*/
		$.drawActorSimpleStatus = function(actor, x, y, width) {
			var lineHeight = this.lineHeight();
			var x2 = x + 180;
			var width2 = Math.max(200, width - Window_Base._faceWidth - this.textPadding());
			this.drawActorName(actor, x, y);
			this.drawActorLevel(actor, x, y  + lineHeight * 2);
			this.drawActorIcons(actor, x, y  + lineHeight * 3);
			this.drawActorClass(actor, x, y  + lineHeight * 1);
			this.drawActorHp(actor, x2, y + lineHeight * 1, width2);
			this.drawActorMp(actor, x2, y + lineHeight * 2, width2);
			if (Param.menuTpGauge) {
				this.drawActorTp(actor, x2, y + lineHeight * 3, width2);
			}
		};

		/*NEW Grid Status*/
		$.drawGridActorStatus = function(actor, x, y, width, height) {
			this.drawActorHpVertical(actor, x, y, width, height);
		};

		$.drawActorHpVertical = function(actor, x, y, height) {
			height = height || 186;
			var color1 = this.hpGaugeColor1();
			var color2 = this.hpGaugeColor2();
			this.drawGaugeVertical(x, y, height, actor.hpRate(), color1, color2);
			this.changeTextColor(this.systemColor());
			this.drawText(TextManager.hpA, x, y, 44);
		};

		/*NEW Draw Text Vertical*/
		$.drawTextVertical = function (text, x, y) {
			var textMax   = text.length;
			for(var i = 0; i < textMax; i++){
				this.drawText(text[i], x, y);
				y += this.lineHeight() - 12;
			}
		};

		/*NEW Draw Gauge Vertical*/
		$.drawGaugeVertical = function(x, y, height, rate, color1, color2) {
			var fillH = Math.floor(height * rate);
			var gaugeW = this.gaugeHeight();
			var gaugeY = y + this.lineHeight() - 8;
			this.contents.fillRect(x, gaugeY, height, 6, this.gaugeBackColor());
			this.contents.gradientFillRect(x, gaugeY, gaugeW, fillH, color1, color2);
		};

		/*NEW drawGaugeOutline*/
		$.drawDarkRect = function (x, y, width, height){
			var color = this.gaugeBackColor();
			this.changePaintOpacity(false);
			this.contents.fillRect(x, y, width, height, color);
			this.changePaintOpacity(true);
		};

		$.isHideGauge = function (actor, meta) {
			var actorId    = actor._actorId;
			var metaResult = $Utils.getMetaData($dataActors[actorId], meta);
			if(metaResult){return true;}
			return false;
		};

	})(Window_Base.prototype, $.Param);

	/**-----------------------------------------------------------------------
     * Window_MenuStatus >>
     *
     *
     ------------------------------------------------------------------------*/
	/**
     * @function
     * @desc This closure function extends and overwrites the Window_MenuStatus class.
     */
	(function($, Param) {

		$.drawItemStatus = function(index) {
			var actor = $gameParty.members()[index];
			var rect = this.itemRect(index);
			var x = rect.x + Window_Base._faceWidth + this.textPadding();
			var y = rect.y;
			var width = rect.width - (x + Param.gaugesWidthTrim) - this.textPadding() * 2;
			this.drawActorSimpleStatus(actor, x, y, width);
		};

	})(Window_MenuStatus.prototype, $.Param);

	/**-----------------------------------------------------------------------
     * Window_SkillStatus >>
     *
     *
     ------------------------------------------------------------------------*/
	/**
     * @function
     * @desc This closure function extends and overwrites the Window_SkillStatus class.
     */
	(function($, Param) {

		$.refresh = function() {
			this.contents.clear();
			if (this._actor) {
				var w = this.width - this.padding * 2;
				var h = this.height - this.padding * 2;
				var y = h / 2 - this.lineHeight() * 2;
				var width = w - (162 + Param.gaugesWidthTrim) - this.textPadding() * 2;
				this.drawActorFace(this._actor, 0, 0, 144, h);
				this.drawActorSimpleStatus(this._actor, 162, y, width);
			}
		};


	})(Window_SkillStatus.prototype, $.Param);


})(LTN.requirePlugins(false, 'WindowCore'));
