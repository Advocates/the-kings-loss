//=============================================================================
//LTN_Layers.js
//=============================================================================
// Version 1.0
/*:
* @plugindesc V1.0 Add extra layers to your maps and battle scenes.
<LTN_Layers>

* @author LTN Games

@param ================
@param  Options
@param ================

@param Layers Folder
@desc Where are your layer images located in your project directory.
@default img/layers

@param Tile Size
@desc Input the size of the tiles you're using
@default 48


@help
================================================================================
▼ TERMS OF USE
================================================================================
Credit goes to: LTN Games

Exclusive to rpgmakermv.co, please don't share anywhere else unless given strict
permission by the author of the plugin.

The plugin may be used in commerical and non-commerical products.
Credit must be given!

Please report all bugs to http://www.rpgmakermv.co/threads/ScriptName

===============================================================================
▼ DEPENDENCIES
===============================================================================
Requires LTN Core.

 ============================================================================
 ▼ DONATIONS
 ============================================================================
Most of the plugins I write are free to use commercially, and I put a lot of
hours into the development of my plugins, If you like my work and want to see
more of it in the future, I ask that you consider offering a donation.

If you do wish to provide your support, you can do so by sending your
contribution via PayPal to: LTNGamesDevelopment(at)gmail.com
===============================================================================
▼ INFORMATION
===============================================================================
This plugin gives you the ability to add layers to your maps & battle.
You can specify which image to appear on map and adjust the movement, opacity
and blend modes of the image loaded. This gives you the oppurtunity to add
special effects like fog and overhead effects to your maps.


===============================================================================
▼ INSTRUCTIONS
===============================================================================

*/

/**-----------------------------------------------------------------------
  * LTNCore Check & Registration Of Plugin >>
  *
  *
 ------------------------------------------------------------------------*/
(function () {
	if (typeof LTN === 'undefined') {
		var strA = "You need to install the LTN Core ";
		var strB = "in order for this plugin to work! Please visit";
		var strC = "\n http://www.rmmv.co/resources/authors/ltn-games.17/";
		var strD = "to download the latest verison.";
		throw new Error(strA + strB + strC + strD);
	} else {
		// Set plugin namespace
		var register = LTN.requireUtils(false, "registerPlugin");
		register('Layers', '1.0', 'LTNGames');
	}
})();

/**-----------------------------------------------------------------------
  * Start Of Plugin >>
  *
  *
 ------------------------------------------------------------------------*/
(function ($) {
	'use strict';
	//Require Utility functions
	//var $Utils = LTN.requireUtils(true);

	$.Parameters = PluginManager.getPluginParameters('LTN_Layers');
	$.Param = $.Param || {};

	$.Param.layerFolder = String($.Parameters['Layers Folder']);
	$.Param.tileSize    = Number($.Parameters['Tile Size']);


	$.Alias.GameInterp_pluginCommand = Game_Interpreter.prototype.pluginCommand;
	Game_Interpreter.prototype.pluginCommand = function(command, args) {
		if (command === 'LAYER'){
			switch(args[0].toLowerCase()) {

					/* Auto adds mapId * LAYER add layer, name, xSpeed, ySpeed, z, opacity, blend */
				case 'add':
					var mapId = $gameMap.mapId();
					if(args[1].toLowerCase() === 'tiling'){
						Layers.createConfig('tiling', mapId, args[2], args[3], args[4] , args[5], args[6], args[7], args[8]);			
					} 
					if(args[1].toLowerCase() === 'static'){
						Layers.createConfig('static', mapId, args[2], args[3], args[4] , args[5], args[6], args[7], args[8]);
					}
					break;

					/* LAYER add mapId layer, name, xSpeed, ySpeed, z, opacity, blend */
				case 'addto':
					if(args[1].toLowerCase() === 'tiling'){
						Layers.createConfig('tiling', args[2], args[3], args[4], args[5] , args[6], args[7], args[8], args[9]);
					} 
					if(args[1].toLowerCase() === 'static'){
						Layers.createConfig('static', args[2], args[3], args[4], args[5] , args[6], args[7], args[8], args[9]);
					}
					break;

					/* LAYER remove mapId, layer */
				case 'remove':
					Layers.removeConfig(args[1], args[2]);
					break;


					/* LAYER refresh remove/add  */
				case 'refresh':
					if(args[1].toLowerCase() === 'remove'){
						SceneManager._scene._spriteset.removeLayers();
					}
					if(args[1].toLowerCase() === 'add'){
						SceneManager._scene._spriteset.addLayers();
					}
					break;
				default:
					break;
			}
		} else {
			$.Alias.GameInterp_pluginCommand.call(this, command, args);
		}
	};
	/**-----------------------------------------------------------------------
     * Image Manager >>
     *
     *
     ------------------------------------------------------------------------*/
	/**
     * @function
     * @desc This closure function extends the ImageManager class, to add in a custom
	 * directory for loading a bitmap.
     */

	ImageManager.loadLayer = function(filename, hue) {
		return this.loadBitmap($.Param.layerFolder, filename, hue, true);
	};
	/**-----------------------------------------------------------------------
     * Spriteset_Base >>
     *
     *
     ------------------------------------------------------------------------*/
	/**
     * @function
     * @desc This closure function extends and overwrites to the Spriteset_Map class
     */

	/*
	* @TODO - Add a removal function, to remove a layer from the scene
	*       - Add a better check for current layers
	*       - Ensure layer won't create a new one if it already exists
	*/
	(function ($, Alias) {

		Alias.SpritesetMap_createLowerLayer = $.createLowerLayer;
		$.createLowerLayer = function() {
			Alias.SpritesetMap_createLowerLayer.call(this);
			var mapId = $gameMap.mapId();
			this._allLayers = this._allLayers || [];
			this.addLayers(mapId);
			this.removeLayers(mapId);
		};

		/**
		* @function
		* @name addLayers
		* @desc Checks if layer don't exist and adds it to children.
		*/
		$.addLayers = function (mapId) {
			/* Assign mapId if not set - For refreshing layers */
			mapId = mapId || $gameMap.mapId();
			var storedLayers = Layers.Storage[mapId];

			for (var layer in storedLayers) {
				var config = Layers.getConfig(mapId, layer);
				/* if layer don't exists create it */
				if(!this.layerExists(layer)){
					this.createLayer(config);
				}
			}
		};

		/**
		* @function
		* @name removeLayers
		* @desc Checks if layer exists and should be removed and removes them.
		*/
		$.removeLayers = function (mapId) {
			/* Assign mapId if not set - For refreshing layers */
			mapId = mapId || $gameMap.mapId();
			var storedLayers = Layers.Storage[mapId];

			for (var layer in storedLayers) {
				var config = Layers.getConfig(mapId, layer);
				/* if layer exists and config is empty remove it*/
				if(this.layerExists(layer) && config.name === ''){
					this.destroyLayer(layer);
				}
			}
		};

		/**
		* @function
		* @name createLayer
		* @desc Adds a layer to children of the tilemap.
		*/
		$.createLayer = function (config) {
			/* if config filename is empty return */
			if(config.name === ''){ return; }
			var layerId = config.layerId;
			switch(config.type) {
				case 'tiling':
					this._allLayers[layerId] = new Layer_Tiling(config);
					this._allLayers[layerId].move(0, 0, Graphics.width, Graphics.height);
					break;
				case 'static':
					this._allLayers[layerId] = new Layer_Static(config);
					break;
				default:
					break;
			}
			this._tilemap.addChild(this._allLayers[layerId]);
		};

		/**
		* @function
		* @name destroyLayer
		* @desc Removes a layer from children and slices from array.
		*/
		$.destroyLayer = function (layerId) {
			var index = this._allLayers.indexOf(this._allLayers[layerId]);
			this._tilemap.removeChild(this._allLayers[layerId]);
			if (index != -1){
				/* Use delete to keep index */
				delete this._allLayers[index];
			}
		};

		/**
		* @function
		* @name layerExists
		* @desc Checks if a layer exists in current layers array '_allLayers'.
		*/
		$.layerExists = function (layer) {
			if(typeof this._allLayers[layer] !== 'undefined'){
				return true;
			}
			return false;
		};


	})(Spriteset_Map.prototype, $.Alias);

	/**-----------------------------------------------------------------------
     * LayersManager >>
     *
     *
     ------------------------------------------------------------------------*/

	/**
     * @name Layers
     * @desc Object containing all layer configuration settings & functions.
	 * call methods using Layers.methodName;
     */

	var Layers =  Layers || {};

	/**
     * @function
	 * @module Layers
     * @desc This closure function contains custom functions for layers
	 *
     */

	(function($){
		/**
         * @name store
         * @desc An object to store all layer configuration settings
         */
		var Storage = Storage || {};
		/**
         * @name config
         * @desc The basic empty configuration for all layers
         */
		var EmptyConfig = {
			type     : '',
			name     : '',
			mapId    : 0,
			layerId  : 0,
			xSpeed   : 0,
			ySpeed   : 0,
			z        : 0,
			opacity  : 0,
			blend    : 0
		};

		/**
		* @function
		* @name add
		* @desc Add layer settings to to a storage container for layer configs.
		*/
		function createConfig (/* type, mapId, layer, name ,xSpeed, ySpeed, z, opacity, blend*/) {
			var config     = {};
			config.type    = arguments[0];
			config.mapId   = Number(arguments[1]);
			config.layerId = Number(arguments[2]);
			config.name    = arguments[3];
			config.xSpeed  = Number(arguments[4]);
			config.ySpeed  = Number(arguments[5]);
			config.z       = Number(arguments[6]);
			config.opacity = Number(arguments[7]);
			config.blend   = Number(arguments[8]);

			/*Push config obj to the config storage*/
			$.storeConfig(config);
			/* Reset values back to default */
			config = EmptyConfig;
		}
		/**
		* @function
		  @name storeConfig
		* @desc Place a confugration object to the storage object.
		* @access private
		*/
		function storeConfig (obj) {
			var id = obj.mapId;
			var layerId = obj.layerId;
			if(typeof Storage[id] !== 'undefined'){
				var storageId = Storage[id];

				if(storageId[layerId] === obj.layerId){
					throw new Error('Configuration already exists for this layer');

				} else {
					/*If current layer does exist, assign layer config to storage*/
					storageId[layerId] = obj;
				}

			} else {
				/*If current layer does not exist, store config with mapId as name
				 * and create first layer.
				 */
				Storage[id] = {};
				Storage[id][layerId] = obj;
			}

		}
		/**
		* @function
		  @name getConfig
		* @desc Get a config from the storage container.
		*/
		function getConfig (id, layer) {
			if(typeof Storage[id] === 'undefined'){
				return;
			}
			if(typeof Storage[id][layer] === 'undefined') {
				return;
			}
			return Storage[id][layer];
		}

		/**
		* @function
		  @name removeConfig
		* @desc Remove a config object in the storage container.
		* @access private
		*/
		function removeConfig (id, layer) {
			if(typeof Storage[id] !== 'undefined'){
				Storage[id][layer] = EmptyConfig;
			}
		}

		/**-----------------------------------------------------------------------
         * Export Layers Config Methods >>
         ------------------------------------------------------------------------*/
		$.Storage = Storage;
		$.createConfig = createConfig;
		$.getConfig    = getConfig;
		$.removeConfig = removeConfig;
		$.storeConfig  = storeConfig;

	})(Layers);

	/**-----------------------------------------------------------------------
     * Sprite_Layers >>
	 *
     * Extends Tilingsprite
     *
     ------------------------------------------------------------------------*/
	/**
     * @function
     * @desc Sprite Layers class is a tiling sprite used for fogs and layers
	 * which move.
     */

	function Layer_Tiling () {
		this.initialize.apply(this, arguments);
	}

	Layer_Tiling.prototype = Object.create(TilingSprite.prototype);
	Layer_Tiling.constructor = Layer_Tiling;

	Layer_Tiling.prototype.initialize = function (config) {
		TilingSprite.prototype.initialize.call(this);
		this.staticX = 0;
		this.staticY = 0;
		this._config = config;
		this.z = this._config.layer;

		this.addLayer();
	};

	Layer_Tiling.prototype.update = function () {
		TilingSprite.prototype.update.call(this);

		this.updatePosition();
	};

	Layer_Tiling.prototype.addLayer = function () {
		this.bitmap  = ImageManager.loadLayer(this._config.name);
		this.z 		   = this._config.z || 0;
		this.opacity   = this._config.opacity || 0;
		this.blendMode = this._config.blend || 0;
	};

	Layer_Tiling.prototype.updatePosition = function () {
		this.origin.x = 0 + this.mapX() * $.Param.tileSize + this.staticX;
		this.origin.y = 0 + this.mapY() * $.Param.tileSize + this.staticY;
		this.staticX += this._config.xSpeed;
		this.staticY += this._config.ySpeed;
	};

	Layer_Tiling.prototype.mapX = function () {
		return $gameMap.displayX();
	};

	Layer_Tiling.prototype.mapY = function () {
		return $gameMap.displayY();
	};

	/**-----------------------------------------------------------------------
     * Sprite_StaticLayer >>
	 *
     * Extends MV Sprite Class
     *
     ------------------------------------------------------------------------*/
	/**
     * @function
     * @desc This closure function contains the custom class Sprite_Layers extended
	 * from the Tilingsprite default MV class.
     */

	function Layer_Static () {
		this.initialize.apply(this, arguments);
	}

	Layer_Static.prototype = Object.create(Sprite.prototype);
	Layer_Static.constructor = Layer_Static;

	Layer_Static.prototype.initialize = function (config) {
		Sprite.prototype.initialize.call(this);
		this._config = config;
		this.xPos = this._config.xSpeed;
		this.yPos = this._config.ySpeed;
		this.addLayer();
	};

	Layer_Static.prototype.update = function () {
		Sprite.prototype.update.call(this);

		this.updatePosition();
	};

	Layer_Static.prototype.addLayer = function () {
		this.bitmap  = ImageManager.loadLayer(this._config.name);
		this.z 		   = this._config.z || 0;
		this.opacity   = this._config.opacity || 0;
		this.blendMode = this._config.blend || 0;
		this.bitmap._setDirty();
	};

	Layer_Static.prototype.updatePosition = function () {
		this.x = this.xPos - this.mapX() * $.Param.tileSize;
		this.y = this.yPos - this.mapY() * $.Param.tileSize;
	};

	Layer_Static.prototype.mapX = function () {
		return $gameMap.displayX();
	};

	Layer_Static.prototype.mapY = function () {
		return $gameMap.displayY();
	};

	/**-----------------------------------------------------------------------
     * Export For Add-ons >>
     ------------------------------------------------------------------------*/
	$.Layers_Config = Layers;
	$.Layer_Tiling  = Layer_Tiling;
	$.Layer_Static  = Layer_Static;

	/**-----------------------------------------------------------------------
     * Save To DataManager >>
     ------------------------------------------------------------------------*/
	DataManager.addToSaveContents('layers', Layers.store);

})(LTN.requirePlugins(false, 'Layers'));
