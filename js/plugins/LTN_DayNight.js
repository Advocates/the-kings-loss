//=============================================================================
//LTN_DayNight.js
//=============================================================================
// Version 1.0
/*:
 *@plugindesc V1.0 Adds a day and night cycle for LTN TimeEngine.
 <LTN_DayNight>

 @author LTN Games

 @param ================
 @param  Tints & Time
 @param ================

 @param Tint speed
 @desc This determines how fast the tint will appear. It will
 gradually increase until it reaches the max time entered here.
 @default 60

 @param Morning Time
 @desc The time for the morning, when the tint will activate.
 @default hour:5

 @param Morning Tint
 @desc The tint color for the morning. rgba values
 @default -34, -17, 10, 68

 @param Day Time
 @desc The time for the day, when the tint will activate.
 @default hour:8

 @param Day Tint
 @desc The tint color for day time.
 @default 0,0,0,0

 @param Noon Time
 @desc The time for the Noon, when the tint will activate.
 @default hour:12

 @param Noon Tint
 @desc The tint color for noon time.
 @default 0,0,0,0

 @param Evening Time
 @desc The time for the evening, when the tint will activate.
 @default hour:17

 @param Evening Tint
 @desc The tint color for evening time.
 @default 17, -34, -68, 17

 @param Night Time
 @desc The time for the night, when the tint will activate.
 @default hour:20

 @param Night Tint
 @desc The tint color for night time.
 @default -102, -85, 0, 170


 @help
================================================================================
▼ TERMS OF USE
================================================================================
Credit goes to: LTN Games

Exclusive to rpgmakermv.co, please don't share anywhere else unless given strict
permission by the author of the plugin.

The plugin may be used in commerical and non-commerical products.
Credit must be given!

Please report all bugs to http://www.rpgmakermv.co/threads/ScriptName

===============================================================================
▼ DEPENDENCIES
===============================================================================
 Requires LTN Core.
 Requires LTN TimeEngine

===============================================================================
▼ INFORMATION
===============================================================================
This plugin is an add-on for "LTN TimeEngine", it will give you options to change
the tint of the game when a certain time is reached.

 ============================================================================
 ▼ DONATIONS
 ============================================================================
Most of the plugins I write are free to use commercially, and I put a lot of
hours into the development of my plugins, If you like my work and want to see
more of it in the future, I ask that you consider offering a donation.

If you do wish to provide your support, you can do so by sending your
contribution via PayPal to: LTNGamesDevelopment@gmail.com

===============================================================================
▼ INSTRUCTIONS
===============================================================================
To use this plugin you first have to setup all tints and times in the
parameters, you may use the default tints and times.

If there is a map you don't want to be affected by the tints, like an indoor map
open the map properties and enter <NoTint> in the notes section. This will turn
off all screen tints.

<NoTint> is case sensitive please ensure you type it exactly as seen here.

*/

(function () {
	if(typeof LTN === 'undefined') {
		var strA = "You need to install the LTN Core ";
		var strB = "in order for this plugin to work! Please visit";
		var strC = "\n http://www.rmmv.co/https://rmmv.co/resources/authors/ltn-games.17/";
		var strD = " to download the latest verison.";
		throw new Error(strA + strB + strC + strD);
	} else {
		var register = LTN.requireUtils(false, "registerPlugin");
		var requiredPlugins = ['TimeEngine'];
		// Set plugin namespace
		register('TE_DayNight','1.0', 'LTNGames', requiredPlugins);
	}
})();

(function($) {
	'use strict';
	/**----------------------------------------------------
	 * IMPORT Plugin(Time Engine) and Utilities from LTNCore >>
	 -------------------------------------------------------*/
	var TimeEngine  = LTN.requirePlugins(false, 'TimeEngine');
	var $Utils      = LTN.requireUtils(true);


	/**----------------------------------------------------
		* Parameters >>
	-------------------------------------------------------*/
	$.Parameters = PluginManager.getPluginParameters('LTN_DayNight');
	$.Param = $.Param || {};
	$.Param.tintSpeed   = Number($.Parameters['Tint Speed']);
	$.Param.morningTint = $Utils.toArray($.Parameters['Morning Tint']);
	$.Param.dayTint     = $Utils.toArray($.Parameters['Day Tint']);
	$.Param.noonTint    = $Utils.toArray($.Parameters['Noon Tint']);
	$.Param.eveningTint = $Utils.toArray($.Parameters['Evening Tint']);
	$.Param.nightTint   = $Utils.toArray($.Parameters['Night Tint']);

	$.Param.morningTime = $Utils.toObj($.Parameters['Morning Time']);
	$.Param.dayTime     = $Utils.toObj($.Parameters['Day Time']);
	$.Param.noonTime    = $Utils.toObj($.Parameters['Noon Time']);
	$.Param.eveningTime = $Utils.toObj($.Parameters['Evening Time']);
	$.Param.nightTime   = $Utils.toObj($.Parameters['Night Time']);



	$.DN_SceneMap_onMapLoaded = Scene_Map.prototype.onMapLoaded;

	Scene_Map.prototype.onMapLoaded = function () {
		$.DN_SceneMap_onMapLoaded.call(this);
		$.notetags = $dataMap.meta;
	};

	$.DN_SceneMap_update = Scene_Map.prototype.update;

	Scene_Map.prototype.update = function () {
		$.dayNight.updateTints();
		$.DN_SceneMap_update.call(this);
	};

	$.DN_GamePlayer_performTransfer = Game_Player.prototype.performTransfer;
	Game_Player.prototype.performTransfer = function() {
		// Tint screen imeddiatly upon transferring.
		$gameScreen.startTint($.dayNight._currentTint || [0, 0, 0, 0], 0);
		$.DN_GamePlayer_performTransfer.call(this);
	};

	/**----------------------------------------------------
	* Time_DayNight >>
	*
	* This class is an extension to Time Engine's game time class.
	* Inheirts from Time Engine 'Game Time' class
	*
	* Setup and update tints automaticaly
	*
	-------------------------------------------------------*/

	function Time_DayNight(){
		this.initialize.apply(this, arguments);
	}
	Time_DayNight.prototype = Object.create(TimeEngine.Game_Time.prototype);
	Time_DayNight.prototype.constructor = Time_DayNight;

	Time_DayNight.prototype.initialize = function () {
		TimeEngine.Game_Time.prototype.initialize.call(this);
		this._super = TimeEngine.gameTime;
		this.clearTint = [0, 0, 0, 0];
		this._noTintMap = false;
		this._tintSpeed = $.Param.tintSpeed || 60;
		this._currentTint = this.clearTint;
		this.setupTints();
		this.setupTintTimes();

	};

	Time_DayNight.prototype.setupTints = function () {
		this.tints    = {morning: $.Param.morningTint,
						 day :    $.Param.dayTint,
						 noon:    $.Param.noonTint,
						 evening: $.Param.eveningTint,
						 night:   $.Param.nightTint};
	};

	Time_DayNight.prototype.setupTintTimes = function () {
		$.Param.morningTime.name = 'morning';
		$.Param.dayTime.name     = 'day';
		$.Param.noonTime.name    = 'noon';
		$.Param.eveningTime.name = 'evening';
		$.Param.nightTime.name   = 'night';
		this.tintTimes = [$.Param.morningTime,
						  $.Param.dayTime,
						  $.Param.noonTime,
						  $.Param.eveningTime,
						  $.Param.nightTime];
	};

	Time_DayNight.prototype.timeOfDay = function () {
		var time = this._super.currentTime();
		var timeOfDay = 'day';
		var tTimes = this.tintTimes;
		var tintsMax = this.tintTimes.length;
		for(var i = 0; i < tintsMax; i++){
			if(time.hour >= Number(tTimes[i].hour)){
				timeOfDay = tTimes[i];
				this._currentTint = this.tints[timeOfDay.name];
			}
		}
		return timeOfDay;
	};


	Time_DayNight.prototype.updateTints = function () {
		if(typeof $.notetags !== 'undefined') {
			if($.notetags.NoTint){
				$gameScreen.startTint(this.clearTint, 0);
				return;
			}
		}
		switch(this.timeOfDay().name) {
			case 'morning':
				$gameScreen.startTint(this.tints.morning, this._tintSpeed);
				break;
			case 'day':
				$gameScreen.startTint(this.tints.day, this._tintSpeed);
				break;
			case 'noon':
				$gameScreen.startTint(this.tints.noon, this._tintSpeed);
				break;
			case 'evening':
				$gameScreen.startTint(this.tints.evening, this._tintSpeed);
				break;
			case 'night':
				$gameScreen.startTint(this.tints.night, this._tintSpeed);
				break;
			default:
				break;
		}
	};

	/**----------------------------------------------------
	 * EXPORT FOR ADD-ONS>>
	 -------------------------------------------------------*/
	$.dayNight = new Time_DayNight();

	DataManager.addToSaveContents('dayNightTints', $.dayNight._currentTint);

}(LTN.requirePlugins(false, 'TE_DayNight')));
